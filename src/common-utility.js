"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.publicApiFunction = void 0;
// Anything exported from this file is importable by other in-browser modules.
function publicApiFunction(displayText, callingMfe) {
    console.log("Exported funtion called from : " + callingMfe);
    window.alert(displayText + ", called from: " + callingMfe);
}
exports.publicApiFunction = publicApiFunction;
