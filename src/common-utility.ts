// Anything exported from this file is importable by other in-browser modules.
export function publicApiFunction(displayText: string, callingMfe: string) {
  console.log("Exported funtion called from : " + callingMfe);
  window.alert(displayText + ", called from: " + callingMfe);
}
